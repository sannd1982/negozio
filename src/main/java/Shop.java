import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Shop {

    private String name;
    private String partitaIva;
    private String numeroTelefono;
    private ArrayList<Product> products;
    private boolean lastInventoryPositive;


    public Shop(String name, String partitaIva, String numeroTelefono) {
        this.name = name;
        this.partitaIva = partitaIva;
        this.numeroTelefono = numeroTelefono;
        this.products = new ArrayList<>();
        this.lastInventoryPositive = true;
    }

    public HashMap<Integer,String> inventory(String filename, String separator) {
        // Accesso al file
        HashMap<Integer,String> errors = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            // Accesso alle righe del file
            String sCurrentLine;
            // Escludo la prima riga
            br.readLine();
            int indexLine = 0;
            // Leggo riga per riga fino a quando trovo del testo
            while ((sCurrentLine = br.readLine()) != null) {
                indexLine++;
                // Estrapolo le informazioni da ciascuna riga mediante carattere divisore
                String[] values = sCurrentLine.split(separator);
                if(values.length != 5){
                    errors.put(indexLine,"Il numero di campi non è corretto");
                    continue;
                }
                String barcode = values[0];
                String name = values[1];
                String description = values[2];
                double price;
                try {
                    price = Double.parseDouble(values[3]);
                }catch(NumberFormatException e) {
                    e.printStackTrace();
                    errors.put(indexLine,e.getMessage());
                    continue;
                }
                int availability = 0;
                try {
                    availability = Integer.parseInt(values[4]);
                }catch(NumberFormatException e) {
                    e.printStackTrace();
                    errors.put(indexLine,e.getMessage());
                    continue;
                }

                Product currentProduct = new Product(barcode, name, description, price);
                currentProduct.setAvailability(availability);
                products.add(currentProduct);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return errors;
    }

    public boolean inventoryCSV(String filename) {
        Reader in = null;
        try {
            in = new FileReader(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Iterable<CSVRecord> records = null;
        try {
            records = CSVFormat.DEFAULT.withHeader().parse(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (CSVRecord record : records) {
            String barcode = record.get("Barcode");
            String name = record.get("Name");
            String description = record.get("Description");
            double price = Double.parseDouble(record.get("Price"));
            int availability = Integer.parseInt(record.get("Availability"));
            Product currentProduct = new Product(barcode, name, description, price);
            currentProduct.setAvailability(availability);
            products.add(currentProduct);
        }
        return true;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public boolean isLastInventoryPositive() {
        return lastInventoryPositive;
    }

    public void setLastInventoryPositive(boolean lastInventoryPositive) {
        this.lastInventoryPositive = lastInventoryPositive;
    }

    public void updateInventory(String productBarcode, int quantity) {
    }


    public boolean isProductAvailable(String barcode, int quantity) {
        Product searchProduct = new Product(barcode);
        if (products.contains(searchProduct)
                && products.get(products.indexOf(searchProduct)).getAvailability() >= quantity) {
            return true;
        }
        return false;
    }

    public boolean aggiornaInventario(String codiceABarre, int quantita) {
        Product trovaProdotto = new Product(codiceABarre);
        ArrayList<Product> prodotti = this.getProducts();
        if (this.isProductAvailable(codiceABarre, quantita)) {
            int posizioneOggettoInventariato = prodotti.indexOf(trovaProdotto);
            Product prodottoInventariato = prodotti.get(posizioneOggettoInventariato);
            int quantitaAttuale = prodottoInventariato.getAvailability();
            prodottoInventariato.setAvailability(quantitaAttuale - quantita);
            return true;
        } else {
            return false;
        }
    }

    public void printAvailability() {
        products.forEach(product -> {
            System.out.println(product.getName() + " - " + product.getAvailability());
        });

    }

}
