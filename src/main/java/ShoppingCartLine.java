public class ShoppingCartLine {

    public ShoppingCartLine(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private Product product;
    private int quantity;

    @Override
    public boolean equals(Object o) {
        if(o instanceof ShoppingCartLine){
            ShoppingCartLine sc = (ShoppingCartLine)o;
            return this.getProduct().equals(sc.getProduct());
        }
        return false;
    }

}
