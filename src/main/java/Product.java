public class Product {

    private String barcode;
    private String name;
    private String description;
    private double price;
    private int availability;

    public Product(String barcode, String name, String description, double price) {
        this.barcode = barcode;
        this.name = name;
        this.description = description;
        this.price = price;
        this.availability = 0;
    }

    public Product(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Product){
            Product p = (Product)o;
            if(this.getBarcode() != null) {
                return this.getBarcode().equals(p.getBarcode());
            }
        }
        return false;
    }
}
