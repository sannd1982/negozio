import java.util.ArrayList;

public class ShoppingCart {
    private String id;
    private double total;
    private ArrayList<ShoppingCartLine> orderLines;
    private Shop shop;

    public ShoppingCart(Shop shop) {
        this.id = "";
        this.total = 0.0;
        orderLines = new ArrayList<>();
        this.shop = shop;

    }


    public boolean addProduct(String productBarcode, int quantity) {
        if (shop.isProductAvailable(productBarcode, quantity) && quantity > 0) {
            this.updateOrderLines(productBarcode, quantity);
            shop.aggiornaInventario(productBarcode, quantity);
            return true;

        }
        return false;
    }

    public void updateOrderLines(String productBarcode, int quantity) {
        Product product = new Product(productBarcode);
        ShoppingCartLine shoppingCartLine = new ShoppingCartLine(product, quantity);
        if (orderLines.contains(shoppingCartLine)) {
            ShoppingCartLine sclToUpdate = orderLines.get(orderLines.indexOf(shoppingCartLine));
            sclToUpdate.setQuantity(sclToUpdate.getQuantity() + quantity);
        } else {
            orderLines.add(shoppingCartLine);
        }
    }

    public boolean removeProduct(String productBarcode, int quantity) {
        // Check prodotto nel carrello esistente
        Product product = new Product(productBarcode);
        ShoppingCartLine shoppingCartLine = new ShoppingCartLine(product, quantity);
        boolean result = false;
        if (orderLines.contains(shoppingCartLine)) {
            ShoppingCartLine sclToUpdate = orderLines.get(orderLines.indexOf(shoppingCartLine));
            int quantityOnCart = sclToUpdate.getQuantity();
            // Check prodotto nel carrello con quantità maggiore o uguale a quella rimossa
            if (quantityOnCart < quantity) {
                return result;
            }
            // Aggiornamento del carrello
            else if (quantityOnCart == quantity) {
                orderLines.remove(sclToUpdate);
                result = true;
            } else {
                sclToUpdate.setQuantity(quantityOnCart - quantity);
                result = true;
            }
        }
        // Aggiornamento inventario
        if (result) {
            this.shop.aggiornaInventario(productBarcode, -quantity);
        }
        return result;
    }

}
