import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProductTest.class,
        ShoppingCartTest.class })

public class SuiteTest {

}