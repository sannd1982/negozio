import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ShoppingCartTest {
    private Shop shop;

    @Before
    public void setupShop(){
        // Instanziamo il negozio
        shop = new Shop("Test", "12321321312", "31231231");
        // Eseguiamo un inventario
        Product vino = new Product("1","Vino","Vino Cannonau Nepente",8.9);
        vino.setAvailability(10);
        ArrayList<Product> inventarioProdotti = new ArrayList<>();
        inventarioProdotti.add(vino);
        shop.setProducts(inventarioProdotti);
    }

    @Test
    public void testAddProductAvailableToShoppingCart(){

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        // Aggiunta di un prodotto presente in negozio con quantità sufficiente
        assertTrue(shoppingCart.addProduct("1",5));
    }

    @Test
    public void testAddProductNotAvailableToShoppingCart() {

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        // Aggiunta di un prodotto presente in negozio con quantità non sufficiente
        assertFalse(shoppingCart.addProduct("1",11));
    }

    @Test
    public void testAddProductNotPresentToShoppingCart() {

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        // Aggiunta di un prodotto presente in negozio con quantità non sufficiente
        assertFalse(shoppingCart.addProduct("2",1));
    }

    @Test
    public void testAddProductWithNegativeQuantityToShoppingCart() {

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        // Aggiunta di un prodotto presente in negozio con quantità non sufficiente
        assertFalse(shoppingCart.addProduct("1",-1));
    }

    @Test
    public void testAddProductWithZeroQuantityToShoppingCart() {

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        // Aggiunta di un prodotto presente in negozio con quantità non sufficiente
        assertFalse(shoppingCart.addProduct("1",0));
    }

    @Test
    public void testAddNullProductToShoppingCart() {

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        // Aggiunta di un prodotto presente in negozio con quantità non sufficiente
        assertFalse(shoppingCart.addProduct(null,0));
    }

    @Test
    public void testAddProductTwiceToShoppingCart() {

        // Istanziamo un carrello
        ShoppingCart shoppingCart = new ShoppingCart(shop);
        shoppingCart.addProduct("1",5);
        // Aggiunta di un prodotto presente in negozio con quantità non sufficiente
        assertFalse(shoppingCart.addProduct("1",6));
    }
}
