import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class ShopTest {

    public Shop shopCorrectInitialized;

    @Before
    public void init(){
        String filename = "/home/daniele/git/negozio_gradle/src/test/docs/inventario.csv";
        shopCorrectInitialized = new Shop("CONAD", "123456", "070000000");
        shopCorrectInitialized.inventory(filename, ",");
    }

    @Test
    public void testInventoryFileFound(){
        assertTrue(shopCorrectInitialized.isLastInventoryPositive());
    }

    @Test
    public void testInventoryFileNotFound(){
        Shop shop = new Shop("CONAD", "123456", "070000000");
        String filename = "/non_esisto/foo.csv";
        HashMap<Integer, String> result = shop.inventory(filename, ",");
        assertTrue(result.isEmpty());
    }

    @Test
    public void testImportCorrectNumberOfLine(){
        int numberOfProducts = shopCorrectInitialized.getProducts().size();
        assertEquals(3, numberOfProducts);
        
    }

    @Test
    public void testCorrectData(){
        Product productToTest = shopCorrectInitialized.getProducts().get(1);
        assertEquals("000000002", productToTest.getBarcode());
        assertEquals("Farina", productToTest.getName());
        assertEquals("Farina Manitoba", productToTest.getDescription());
        assertEquals(0.85, productToTest.getPrice(),0.001);
        assertEquals(10, productToTest.getAvailability());
    }



}
